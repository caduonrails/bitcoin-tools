# Address Generator

## Prerequisites

1. [Install `nodejs` and `yarn`](https://yarnpkg.com/)
1. Install the package dependencies: `yarn install`

## Generating accounts

Run the `generator.js` script to create accounts. Parameters are `[private keys target] [addresses target] [public addresses target] [number of keys to be created]`:

```
node generator.js /where/to/store/private-keys.txt /where/to/store/addresses.txt /where/to/store/public-addresses.txt number-of-keys-to-generate 
```

Use -p if you want to see the progress on the created addresses.

The files `private-keys.txt`, `addresses.txt` and `public-addresses.txt` will contain one private key, one address and one public key per line. In every file, the address in the nth line corresponds to the public and private key also in the nth line.


