const bitcore = require("bitcore-lib");
const fs = require("fs");

const generatePrivateKeys = (n) => {
  const keys = [];

  for (let i = 0; i < n; i++) {
    keys.push(new bitcore.PrivateKey().toString());
  }

  return keys;
}

const createAccounts = (n) => {
  const privateKeys = generatePrivateKeys(n);

  const addresses = privateKeys.map(pk => bitcore.PrivateKey(pk).toAddress().toString());

  fs.writeFileSync("private-keys.json", JSON.stringify(privateKeys));
  fs.writeFileSync("addresses.json", JSON.stringify(addresses));
}

createAccounts(process.argv[2]);
