const explorers = require("bitcore-explorers");
const insight = new explorers.Insight();
const fs = require("fs");

let addresses = JSON.parse(fs.readFileSync("addresses.json"));

const fetchUTXOs = () => {
  const utxos = addresses.map(address => insight.getUnspentUtxos(address, function(err, utxos) {
    if (err) {
      console.error(err);
    }
    else {
      fs.writeFileSync("utxos.json", JSON.stringify(utxos));
    }
  })
  );
  console.log(utxos);
};

fetchUTXOs();
