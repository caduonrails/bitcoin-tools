const fs = require("fs")
const bitcore = require("bitcore-lib")
const args = process.argv.slice(2)
const addressesFile = args[0]
const publicKeysFile = args[1]
const privateKeysFile = args[2]
const amountsFile = args[3]
const poolOrRefundFile = args[4]
const utxo = {
  "txId" : "115e8f72f39fad874cfab0deed11a80f24f967a84079fb56ddf53ea02e308986",
  "outputIndex" : 0,
  "address" : "17XBj6iFEsf8kzDMGQk5ghZipxX49VXuaV",
  "script" : "76a91447862fe165e6121af80d5dde1ecb478ed170565b88ac",
  "satoshis" : 50000
}

let privateKeys = fs.readFileSync(privateKeysFile).toString().split('\n')
let addresses = fs.readFileSync(addressesFile).toString().split('\n')
let publicKeys= fs.readFileSync(publicKeysFile).toString().split('\n')
let amounts = fs.readFileSync(amountsFile).toString().split('\n')
let poolOrRefund = fs.readFileSync(poolOrRefundFile).toString().split('\n')

var privateKey = new bitcore.PrivateKey('L1uyy5qTuGrVXrmrsvHWHgVzW9kKdrp27wBC7Vs6nZDTF2BRUVwy');

var transaction = new bitcore.Transaction()
  .from(utxo)
  .to(addresses[0].toString(), 7000)
  .sign(privateKey);

console.log(transaction)
